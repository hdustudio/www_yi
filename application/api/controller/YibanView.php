<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/12
 * Time: 11:19
 */

namespace app\api\controller;

use think\Cache;
use think\Config;
use think\Controller;
use think\Response;
use think\Session;
//header('text/html');
class YibanView extends Controller
{


    protected $yiban='23ea826d9d185c2c';

    public function index(){
        //return $this->removeAuth(16164885);
        return $this->auth();
    }
    protected function check(){
        $postObject = addslashes($_GET["verify_request"]);

        $postStr = pack("H*", $postObject);
        $appID = "23ea826d9d185c2c";//应用appID
        $appSecret = "594a504311fee0b8d4de63b541fec1dd";//应用appSecret
        $postInfo = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $appSecret, $postStr, MCRYPT_MODE_CBC, $appID);
       // mcrypt_decrypt('','','','','');
       // $postInfo = openssl_encrypt($postStr,'AES-256-CBC',$appSecret,OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING,$appID);
        $postInfo = rtrim($postInfo);
        $res = json_decode($postInfo,true);
        $acess_token = $res['visit_oauth']['access_token'] ;
        $token_expires = $res['visit_oauth']['token_expires'] ;
        $user_id = $res['visit_user']['userid'];
        Session::set($user_id, $acess_token);
        dumpnice($res);


    }
    protected function auth(){
        return redirect('https://openapi.yiban.cn/oauth/authorize?client_id=23ea826d9d185c2c&redirect_uri=http://f.yiban.cn/iapp232209&state=222');

    }

    protected function user($access_token){

        $url = 'https://openapi.yiban.cn/user/me?access_token='.$access_token;
        return curl_get_https($url);
    }
    protected function view(){
        return $this->fetch('Yiban/SignIm');
    }
    //检测是否授权
    protected function checkAuth(){

        $url = 'https://openapi.yiban.cn/oauth/token_info';
        $param = ['client_id'=>$this->yiban];
        $res = json_decode( request_post($url,$param),true);
        $status = $res['status'];
        dump($res);
        if($status = 404) return '未授权';
        if($status = 200) return 'yishouquan';
    }

    protected function checkIsVerify($stu_id){
        $url = 'https://openapi.yiban.cn/user/is_verify';
        $param = array();
        $param =[
          'access_token'=>Session::get($stu_id),
          'school_name'=>'杭州电子科技大学',
          'verify_key'=>'student_id',
          'verify_value'=>'',
        ];
        $request = $url.'?'.http_build_query($param);
        return curl_get_https($request);
    }
    //个人信息模块
    protected function personInfo($stu_id){
        $person = Model('person');
        //找到该学生
        $person::get(['number'=>$stu_id]);
        //输出信息

        return ;
    }
    //解除授权
    protected function removeAuth($user_id){
        $url = 'https://openapi.yiban.cn/oauth/revoke_token';
        $param = [
            'client_id'=>$this->yiban,
            'access_token'=>Session::get($user_id)
        ];
        $res = request_post($url,$param);
        if(json_decode($res,true)['status']==200){
            Session::clear();
            return 200;
        }else
            return 500;
    }

    //检测是否已经签到
    protected function isCheck(){

    }

    //获取教室
    protected function getClass($class_id){
        $class = model('ClassConfig');
        $result = $class::get(['class_id'=>$class_id]);
        return $result;
    }

    //设置课程随机码
    protected function setCode($class_id){
        $code = random_code();
        $class_code = model('ClassCode');
        $class_code->setCode($class_id,$code);
        Cache::set($class_id,$code,300);
        return $code;
    }

    //检测地理位置
    protected function getLocation(){

    }


    //计算误差
    /**
     * @param $class_id
     * @param array $locations
     * @param int $scale
     * @return int
     */
    protected function checkLocation($class_id,$locations=[],$scale=30){
        $class = model('ClassCode');
        $location =  json_decode($class->checkLocation($class_id),true);
        $lati = $location['lati'];
        $longti = $location['longti'];
        $realLati = $locations['lati'];
        $realLongti = $locations['longti'];
        $realScale = getDistance($longti,$lati,$realLongti,$realLati);
        if($realScale<$scale){
            return 1;
        }
        else
            return 0;
    }

    //检测是否开课
    protected function isStart($class_id){
        $class = model('ClassCode');
        $result =  json_decode($class->m_isStart($class_id),true);
        return $result['isStart'];
    }

    //检测随机码是否正确
    protected function checkClassCode($class_id){
       // $stu_code = $this->request->post('code');
        $stu_code = '';
        $sys_code = Cache::get($class_id);
        if($stu_code === $sys_code)
            return Response::create('正确','','200');
        else
            return Response::create('验证码不正确','','500');
    }

}