<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/14
 * Time: 17:18
 */

namespace app\api\model;


use think\Model;

class ClassCode extends Model
{
    protected function initialize()
    {
        //需要调用`Model`的`initialize`方法
        parent::initialize();
        //TODO:自定义的初始化
    }

    protected $name = 'class_code';

    public function setCode($class_id, $code)
    {
        $data = [
            'code' => $code,
        ];
        self::where('class_id', $class_id)->setField($data);
    }

  //返回地理位置
    public function checkLocation($class_id)
    {
        $result = self::where('class_id', $class_id)->field('class_id,lati,longti')->select();
        return $result[0];
    }
    //返回是否开始
    public function m_isStart($class_id){
        $result = self::where('class_id', $class_id)->field('isStart')->select();
        return $result[0];
    }
}