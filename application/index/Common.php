<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/7
 * Time: 12:25
 */

namespace app\index;


use app\index\model\Login;
use think\Cache;
use think\Cookie;
use think\Loader;
use think\Session;

class Common
{
    public static function arr2show($arr=[]){
        foreach ($arr as $value){
            echo $value.PHP_EOL;
        }
    }
    public static function getToken(){
        $request = \think\Request::instance();
        return $request ->token('__token__','sha1');
    }
    public static function arrayreturn($methed=false,$msg=''){
        $res['result'] =$methed;
        $res['msg'] =$msg;
        return $res;
    }

    public function auth(){
        $cookie = Cookie::get('PHPSESSIsD');
        $session = isset($cookie)?Session::get($cookie):NULL;
        return !$session&&!isset($cookie);
    }
    public static function random_code($length = 6){
    $min = pow(10 , ($length - 1));
    $max = pow(10, $length) - 1;
    return rand($min, $max);
    }
    /**
     * 检测手机短信验证码
     * #User: Mikkle
     * #Email:776329498@qq.com
     * #Date:
     * @param $mobile
     * @param bool|false $code
     * @return bool
     */
    protected function checkRegSms($mobile, $code = false)
    {
        if (!$mobile) return false;
        if ($code === false) {   //判断60秒以内是否重复发送
            if (!Cache::has('sms_' . $mobile)) return true;
            if (Cache::get('sms_' . $mobile)['times'] > time()) {
                return false;
            } else {
                return true;
            }
        } else {  //判断验证码是否输入正确
            if (!Cache::has('sms_' . $mobile)) return false;
            if (Cache::get('sms_' . $mobile)['code'] == $code) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 设置手机短息验证码缓存
     * #User: Mikkle
     * #Email:776329498@qq.com
     * #Date:
     * @param $data_cache
     */
    protected function setRegSmsCache($data_cache)
    {
        Cache::set('sms_' . $data_cache['mobile'], 300);
    }
    /**
     * 系统邮件发送函数
     * @param string $tomail 接收邮件者邮箱
     * @param string $name 接收邮件者名称
     * @param string $subject 邮件主题
     * @param string $body 邮件内容
     * @param string $attachment 附件列表
     * @return boolean
     * @author static7 <static7@qq.com>
     */
    public function send_mail($tomail, $name, $subject = '', $body = '', $attachment = null) {
        $mail = new \PHPMailer();           //实例化PHPMailer对象
        $mail->CharSet = 'UTF-8';           //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
        $mail->IsSMTP();                    // 设定使用SMTP服务
        $mail->SMTPDebug = 0;               // SMTP调试功能 0=关闭 1 = 错误和消息 2 = 消息
        $mail->SMTPAuth = true;             // 启用 SMTP 验证功能
        $mail->SMTPSecure = 'ssl';          // 使用安全协议
        $mail->Host = "smtp.exmail.qq.com"; // SMTP 服务器
        $mail->Port = 465;                  // SMTP服务器的端口号
        $mail->Username = "static7@qq.com";    // SMTP服务器用户名
        $mail->Password = "";     // SMTP服务器密码
        $mail->SetFrom('static7@qq.com', 'static7');
        $replyEmail = '';                   //留空则为发件人EMAIL
        $replyName = '';                    //回复名称（留空则为发件人名称）
        $mail->AddReplyTo($replyEmail, $replyName);
        $mail->Subject = $subject;
        $mail->MsgHTML($body);
        $mail->AddAddress($tomail, $name);
        if (is_array($attachment)) { // 添加附件
            foreach ($attachment as $file) {
                is_file($file) && $mail->AddAttachment($file);
            }
        }
        return $mail->Send() ? true : $mail->ErrorInfo;
    }

}