<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/6
 * Time: 19:51
 */

namespace app\index\validate;
use think\Validate;
use think\Session;
class Login extends Validate
{
    protected $rule =[
        '__token__' =>'require|token',
        'username'=>'require',
        'password'=>'require|saltlength',
        'captcha|验证码'=>'require|captcha',
    ];
    protected $field=[
        'username'=>'用户名',
        'password'=>'密码',
    ];
   /**protected function tokens($value){
        $checkToken = Session::get('__token__');
        return $value == $checkToken? true:'令牌错误';
    }**/
    protected function saltlength($value){
       return $value !== '69b1d3057e00bd0731d4576f836a3904'?true:'密码不得为空';
    }



}