<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/6
 * Time: 12:20
 */
namespace app\index\validate;
use think\Debug;
use think\Validate as Va;
class Validate extends Va
{

        protected $rule = [
            ['name','require|max:25','名称必须|名称最多不能超过25个字符'],
            'age|年龄'   => 'number|between:1,120',
            'email' => 'email',
        ];
        protected $msg = [
            'age.number'   => '{%age_number}',
            'age.between'  => '年龄只能在1-120之间',
            'email'        => '邮箱格式错误',
        ];
        protected   $field = [
            'name'  => '名称',
            'email' => '邮箱',
        ];
        protected function email($value,$rule){
            return $value == $rule ? true:'错误';
        }
        protected $scene = [
            'edit'  =>  ['name','age'=>'require|number|between:1,5'],
        ];


}