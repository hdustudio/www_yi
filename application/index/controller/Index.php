<?php
namespace app\index\controller;
use app\index\model\User;
use think\Cache;
use think\Controller;
use think\Cookie;
use think\Model;
use think\Session;
use app\index\model\Login;
use app\index\Common;
use app\index\model\SendSms;
use think\Response;
class Index extends Controller
{
    public function index()
    {
        return '<style type="text/css">*{ padding: 0; margin: 0; } .think_default_text{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p> ThinkPHP V5<br/><span style="font-size:30px">十年磨一剑 - 为API开发设计的高性能框架</span></p><span style="font-size:22px;">[ V5.0 版本由 <a href="http://www.qiniu.com" target="qiniu">七牛云</a> 独家赞助发布 ]</span></div><script type="text/javascript" src="http://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script><script type="text/javascript" src="http://ad.topthink.com/Public/static/client.js"></script><thinkad id="ad_bd568ce7058a1091"></thinkad>';
    }
    public function va(){
        $data = [
            'name'  => '',
            'age'   => 'sda',
            'email' => 'thinkphp',
        ];
        $validate = $this->validate($data,'validate.edit','',true);
       dump($validate);
    }
    public function token(){
        $cookie = Cookie::get('PHPSESSIsD');
        $session = isset($cookie)?Session::get($cookie):NULL;
        $auth = new Common();
        if($auth->auth()){
            $token = $this -> request ->token('__token__','sha1');
            $this->assign('token',$token);
            return $this->fetch('index');
        }else{
            $this->assign('username',$session);
            return $this->fetch('admin');
        }
    }
    public function test(){
       Cache::clear();
    }
    public function register()
    {
        $data = array();
        $data['__token__']=$this->request->post('__token__');
        $data['username'] = $this->request->post('username');
        $data['password'] = $this->request->post('password');
        $data['captcha'] = $this->request->post('captcha');
        $login = new Login();
        $check = $login->reg($data);
        if($check['result']){
            $this->assign('username',$data['username']);
            return $this->fetch('admin');
        }else{
            return \response($check['msg'],500);
        }
    }
    public function login_out(){
            session::clear();
            Cookie::delete('PHPSESSIsD');
            $this->success('退出成功','token');
    }
    //修改手机
    public function bind_telephone(){
    }
    //修改密码
    public function update_all()
    {
        $model = new User();
        $name = input('username');
        $password = input('new_password');
        $sms_code = input('sms_code');
        $telphone = input('telphone');
        $check_result = $this->checkRegSms($telphone,$sms_code,1);

        if($check_result){
            $model->where(['username'=>$name])
                ->update(['password'=>$password]);
            $change = Model('change');
            $upd = $change->data(['username'=>$name,'new_password'=>$password,'pwd_ip'=>request()->ip()])->save();
            if($upd){
                session::clear();
                Cookie::delete('PHPSESSIsD');
                $this->success('修改成功','index/token');
            }else{
                return '修改失败';
            }
        }else return json(['code'=>'','msg'=>'']);

    }
    //产生验证码并写入缓存
    public function setSmsCode(){
        $num = $this->request->post('telephone');
        dump($num);
        $code = Common::random_code();
        $send = $this->checkRegSms($num,$code,2);
        $data=[
            'mobile'=>$num,
            'code'=>$code,
            "times"=>time(),
        ];

        if($send){
            $this->setRegSmsCache($data);
        }else  return Response::create('间隔过短','','500');

        $sms = SendSms::sendSms($num,'文小二工作室','SMS_134630008',array('code'=>$code));
        return dump($sms);
    }
    protected function checkRegSms($mobile, $code = false,$method = 1)
    {
       if (empty($mobile)) return false;
       if($method == 1) {
         //判断60秒以内是否重复发送
               if (!Cache::has('sms_' . $mobile)) return false;
               if (Cache::get('sms_' . $mobile)['times'] > time()) return false;
             //判断验证码是否输入正确
               if (Cache::get('sms_' . $mobile)['code'] == $code) {
                   return true;
               } else {
                   return false;
               }

       }
          if($method == 2) {
              if ($code !== false) {   //判断60秒以内是否重复发送
                  if (!Cache::has('sms_' . $mobile)) return true;
                  if (Cache::get('sms_' . $mobile)['times'] > time()+60) {

                      return true;
                  } else {
                      echo '1';
                      return false;
                  }
              }
          }
        return false;


    }

    /**
     * 设置手机短息验证码缓存
     * #User: Mikkle
     * #Email:776329498@qq.com
     * #Date:
     * @param $data_cache
     */
    protected function setRegSmsCache($data_cache)
    {
        Cache::set('sms_' . $data_cache['mobile'], $data_cache,300);
    }


}
