<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/8
 * Time: 21:40
 */

namespace app\index\controller;
use think\Controller;
class View extends Controller
{
    public function chengepwd(){
        return $this->fetch('index/changepwd');
    }
    public function bindtelephone(){
        return $this->fetch('index/telephone');
    }
}