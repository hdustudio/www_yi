<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/8
 * Time: 13:42
 */

namespace app\index\model;
use think\Model;
use app\index\Common;
class Change extends Model
{
    protected $name = 'user_changepwd';
    //记录设置时间
    protected $autoWriteTimestamp = 'datetime';
    protected $updateTime = false;
}