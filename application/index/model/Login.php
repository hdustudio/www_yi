<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/6
 * Time: 20:40
 */

namespace app\index\model;
use think\Model;
use app\index\validate\Login as LoginValidate;
use app\index\Common;
use think\Cache;
use think\Cookie;
use think\Response;
use think\Session;
use app\index\model\UserLogin;
class Login extends Model
{
    public function reg($data=[]){
        return self::register($data);
    }
    protected $name = "user";
    protected static function register($data=[]){
        $validate = new LoginValidate();
        $users = Model('UserLogin');
            if($validate->batch()->check($data)){
                $user = self::get(['username'=>$data['username']]);
                if($user){
                    $password_check = $user['password'] == $data['password'] ? true:false;
                    if($password_check) {

                        $users ->setLoginIP($user['username'],1);
                        return self::checkOK($user['username']);
                    }
                    else{
                        $users ->setLoginIP($user['username'],0);
                        return Common::arrayreturn(false,'密码错误');
                    }
                }else{
                    $users ->setLoginIP($user['username'],0);
                    return Common::arrayreturn(false,'用户不存在');
                }
            }else{
                $users ->setLoginIP($user['username'],0);
                return Common::arrayreturn(false,dump(Common::arr2show($validate->getError())));
            }
    }
    //set cookie cache
    protected static function checkOK($username){
      try{
            $phpsession = Cookie::get('PHPSESSID');
            Cookie::set('PHPSESSIsD',$phpsession,3600);
            Session::set($phpsession,$username);
            return Common::arrayreturn(true,'');
      }catch (\Exception $exception){
            return Common::arrayreturn(false,$exception);
      }

    }


}