<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/8
 * Time: 13:15
 */

namespace app\index\model;
use think\Model;
class User extends Model
{
    protected $name = 'user';
    //修改密码
    public  function changePwd(){

    }
    //绑定手机
    public  function bindTel(){

    }
    //查询是否绑定手机
    public  function searchBindTel(){

    }
    //改变手机号
    public  function changeTel(){

    }
    //改变用户信息
    public  function changePersonConfig(){

    }
    //绑定hdu
    public  function bindHdu(){

    }
    //查询是否绑定hdu
    public  function searchBindHdu(){

    }
    //关联changepwd
    public function change(){
        return $this->hasOne('Change','username','id');
    }
}