<?php
/**
 * Created by PhpStorm.
 * User: hong
 * Date: 2018/5/8
 * Time: 13:43
 */

namespace app\index\model;

use think\Loader;
use think\Model;
use app\index\Common;
class UserLogin extends Model
{
    protected $name = 'user_login';

    //记录ip

    public function setLoginIP($username,$result)
    {

        $ip = request()->ip();
        self::data(['username'=>$username,'login_result'=>$result,'login_ip'=>$ip])->validate([
                'username'=>'require','login_result'=>'require|number:1'])->save();
    }

    //记录登陆时间
    protected $autoWriteTimestamp = 'datetime';
    protected $updateTime = false;
}